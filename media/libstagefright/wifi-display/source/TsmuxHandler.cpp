/*
 * Copyright 2012, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//#define LOG_NDEBUG 0
#define LOG_TAG "TsmuxHandler"
#include <utils/Log.h>

#include "TsmuxHandler.h"
#include "TsmuxSender.h"
#include "ANetworkSession.h"

#include <media/stagefright/foundation/ABuffer.h>
#include <media/stagefright/foundation/ADebug.h>
#include <media/stagefright/foundation/AMessage.h>
#include <media/stagefright/MediaBuffer.h>
#include <media/MediaSource.h>
#include <media/stagefright/MetaData.h>

#include "tsmux_hal.h"

#define TSMUX_MAX_M2M_CMD_QUEUE_NUM 3

namespace android {

TsmuxHandler::TsmuxHandler(
        void *tsmuxHandle,
        sp<TsmuxSender> tsmuxSender,
        enum Mode mode,
        uint32_t width,
        uint32_t height)
    : mTsmuxHandle(tsmuxHandle),
      mMode(mode),
      mTsmuxSender(tsmuxSender),
      mWidth(width),
      mHeight(height) {
    ALOGI("created TsmuxHandler (mode: %d)", mMode);

    if (mode == M2M_MODE)
        tsmux_init_m2m(mTsmuxHandle);
    else
        tsmux_init_otf(mTsmuxHandle, mWidth, mHeight);

    mIsStopped = false;
    mLastPSITimeUs = 0;
}

TsmuxHandler::~TsmuxHandler() {
    if (mMode == M2M_MODE)
        tsmux_deinit_m2m(mTsmuxHandle);
    else
        tsmux_deinit_otf(mTsmuxHandle);
}

status_t TsmuxHandler::startOTF() {
    ALOGI("start tsmux OTF");

    sp<AMessage> msg = new AMessage(kWhatStartOTF, this);
    msg->post();

    return OK;
}

status_t TsmuxHandler::stopOTF() {
    ALOGI("stop tsmux OTF");

    sp<AMessage> msg = new AMessage(kWhatStopOTF, this);
    sp<AMessage> response;
    status_t err = msg->postAndAwaitResponse(&response);

    if (err != OK) {
        return err;
    }

    if (!response->findInt32("err", &err)) {
        err = OK;
    }
    return err;
}

void TsmuxHandler::packetizeM2M(sp<ABuffer> &inBuf) {
    ALOGV("tsmux packetizeM2M");
    {
        Mutex::Autolock autoLock(mLockM2MInputBuffers);
        mM2MInputBuffers.push_back(inBuf);
    }
    sp<AMessage> msg = new AMessage(kWhatPacketizeM2M, this);
    msg->post();
}

status_t TsmuxHandler::setTsmuxConfigDataOTF() {
    ALOGV("tsmux setTsmuxConfigDataOTF");

    struct tsmux_config_data config;

    status_t err = tsmux_get_config_otf(mTsmuxHandle, &config);

    if (err != OK)
        return err;

    // set pkt_ctrl
    config.pkt_ctrl->psi_en = 0;
    config.pkt_ctrl->rtp_seq_override = 0;
    config.pkt_ctrl->pes_stuffing_num = 0;
    config.pkt_ctrl->mode = 1;
    config.pkt_ctrl->id = 0;

    // set pes_hdr
    config.pes_hdr->code = PES_HDR_CODE;
    config.pes_hdr->stream_id = 0xe1;
    config.pes_hdr->pkt_len = 0;
    config.pes_hdr->marker = PES_HDR_MARKER;
    config.pes_hdr->scramble = 0;
    config.pes_hdr->priority = 0;
    config.pes_hdr->alignment = 0;
    config.pes_hdr->copyright = 0;
    config.pes_hdr->original = 0;
    config.pes_hdr->flags = 0;
    config.pes_hdr->hdr_len = 0;

    // set ts_hdr
    config.ts_hdr->sync = TS_HDR_SYNC;
    config.ts_hdr->error = 0;
    config.ts_hdr->priority = 1;
    config.ts_hdr->pid = 0x1011;
    config.ts_hdr->scramble = 0;
    config.ts_hdr->adapt_ctrl = 0;

    // set rtp_hdr
    config.rtp_hdr->ver = 0;
    config.rtp_hdr->pad = 0;
    config.rtp_hdr->ext = 0;
    config.rtp_hdr->csrc_cnt = 0;
    config.rtp_hdr->marker = 0;
    config.rtp_hdr->pl_type = 0;
    config.rtp_hdr->seq = 0;
    config.rtp_hdr->ssrc = 0;

    return err;
}

status_t TsmuxHandler::setTsmuxConfigDataM2M() {
    struct tsmux_config_data config;

    status_t err = tsmux_get_config_otf(mTsmuxHandle, &config);

    if (err != OK)
        return err;

    // set pkt_ctrl
    config.pkt_ctrl->psi_en = 0;
    config.pkt_ctrl->rtp_seq_override = 0;
    config.pkt_ctrl->pes_stuffing_num = 0;
    config.pkt_ctrl->mode = 0;
    config.pkt_ctrl->id = 0;

    // set pes_hdr
    config.pes_hdr->code = PES_HDR_CODE;
    config.pes_hdr->stream_id = 0xe1;
    config.pes_hdr->pkt_len = 0;
    config.pes_hdr->marker = PES_HDR_MARKER;
    config.pes_hdr->scramble = 0;
    config.pes_hdr->priority = 0;
    config.pes_hdr->alignment = 0;
    config.pes_hdr->copyright = 0;
    config.pes_hdr->original = 0;
    config.pes_hdr->flags = 0;
    config.pes_hdr->hdr_len = 0;

    // set ts_hdr
    config.ts_hdr->sync = TS_HDR_SYNC;
    config.ts_hdr->error = 0;
    config.ts_hdr->priority = 1;
    config.ts_hdr->pid = 0x1011;
    config.ts_hdr->scramble = 0;
    config.ts_hdr->adapt_ctrl = 0;

    // set rtp_hdr
    config.rtp_hdr->ver = 0;
    config.rtp_hdr->pad = 0;
    config.rtp_hdr->ext = 0;
    config.rtp_hdr->csrc_cnt = 0;
    config.rtp_hdr->marker = 0;
    config.rtp_hdr->pl_type = 0;
    config.rtp_hdr->seq = 0;
    config.rtp_hdr->ssrc = 0;

    return err;
}

void TsmuxHandler::onMessageReceived(const sp<AMessage> &msg) {
    switch (msg->what()) {
        case kWhatStartOTF:
        {
            ALOGI("onMessageReceived: kWhatStartOTF");

            mIsStopped = false;

            sp<AMessage> msg = new AMessage(kWhatRunOTF, this);
            msg->post();

            break;
        }

        case kWhatStopOTF:
        {
            ALOGI("onMessageReceived: kWhatStopOTF");
            sp<AReplyToken> replyID;
            status_t err = OK;

            mIsStopped = true;

            CHECK(msg->senderAwaitsResponse(&replyID));

            sp<AMessage> response = new AMessage;
            response->setInt32("err", err);
            response->postReply(replyID);

            break;
        }

        case kWhatRunOTF:
        {
            sp<ABuffer> outbuf;

            if (mIsStopped) {
                ALOGI("tsmux OTF is stopped");
                break;
            }

            if (setTsmuxConfigDataOTF() != OK) {
                ALOGE("setTsmuxConfigDataOTF fail");
                break;
            }

            ALOGV("tsmux otf dq buf");

            status_t err = tsmux_dq_buf_otf(mTsmuxHandle, outbuf);

            if (err == OK && outbuf != NULL) {
                outbuf->meta()->setInt32("mode", OTF_MODE);
                mTsmuxSender->sendRTP(outbuf);
                tsmux_q_buf_otf(mTsmuxHandle);
            }

            sp<AMessage> msg = new AMessage(kWhatRunOTF, this);
            msg->post();
            break;
        }

        case kWhatPacketizeM2M:
        {
            Mutex::Autolock autoLock(mLockM2MInputBuffers);
            if (mM2MInputBuffers.isEmpty())
                break;

            int bufferCount = mM2MInputBuffers.size();
            if (bufferCount > TSMUX_MAX_M2M_CMD_QUEUE_NUM)
                bufferCount = TSMUX_MAX_M2M_CMD_QUEUE_NUM;
            /* WA: we have to find next ts continuity counter in tsmux output */
            bufferCount = 1;
            sp<ABuffer> inbufs[TSMUX_MAX_M2M_CMD_QUEUE_NUM] = {NULL, };
            sp<ABuffer> outbufs[TSMUX_MAX_M2M_CMD_QUEUE_NUM] = {NULL, };
            for (int i = 0; i < bufferCount; i++)
                inbufs[i] = mM2MInputBuffers.itemAt(i);

            tsmux_packetize_m2m(mTsmuxHandle, inbufs, outbufs);

            for (int i = 0; i < bufferCount; i++) {
                if (outbufs[i] != NULL) {
                    outbufs[i]->meta()->setInt32("mode", M2M_MODE);
                    mTsmuxSender->sendRTP(outbufs[i]);
                }

                mM2MInputBuffers.removeAt(0);
            }

            break;
        }

        default:
            TRESPASS();
    }
}

}  // namespace android
