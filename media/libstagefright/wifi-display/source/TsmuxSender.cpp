/*
 * Copyright 2012, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//#define LOG_NDEBUG 0
#define LOG_TAG "TsmuxSender"
#include <utils/Log.h>

#include "TsmuxHandler.h"
#include "TsmuxSender.h"
#include "tsmux_hal.h"
#include "ANetworkSession.h"

#include <cutils/properties.h>
#include <media/stagefright/foundation/ABuffer.h>
#include <media/stagefright/foundation/ADebug.h>
#include <media/stagefright/foundation/AMessage.h>
#include <media/stagefright/MediaBuffer.h>
#include <media/MediaSource.h>
#include <media/stagefright/MetaData.h>

#define TSMUX_MAX_M2M_CMD_QUEUE_NUM 3

namespace android {

TsmuxSender::TsmuxSender(
        sp<MediaSender> mediaSender, const sp<ANetworkSession> &netSession)
    : mMediaSender(mediaSender),
      mNetworkSession(netSession) {
    mLastRTPSequenceNumber = -1;
    ALOGI("created TsmuxSender");
    mLastG2dStart = 0;
    mDumpFile = NULL;
    mLastPatCC = -1;
    mLastPmtCC = -1;
    mLastVideoCC = -1;
    mLastAudioCC = -1;
    mLogLevel = 0;
    char value[PROPERTY_VALUE_MAX];
    property_get("wfd.lowlatency.loglevel", value, "0");
    int logLevel = atoi(value);
    if (logLevel == 0)
        mLogLevel = 0;
    else
        mLogLevel = 1;
    ALOGI("wfd low latency log level %d", mLogLevel);
}

TsmuxSender::~TsmuxSender() {
}

void TsmuxSender::sendRTP(sp<ABuffer> rtpBuffer) {
    sp<AMessage> msg = new AMessage(kWhatTransmitRTP, this);
    msg->setBuffer("rtpBuffer", rtpBuffer);
    msg->post();
}

void TsmuxSender::init()
{
    sp<AMessage> msg = new AMessage(kWhatInit, this);
    msg->post();
}

void TsmuxSender::transmitRTP(sp<ABuffer> rtpBuffer)
{
    char *rtp = (char *)rtpBuffer->data();
    int remain_rtp_size = rtpBuffer->size();
    int rtp_size = 0;
    int ts_pkt_count_per_rtp = 0;
    int rtpSessionID;
    int mode;
    int rtp_seq_num_start = -1;
    int rtp_seq_num_end = -1;
    int ts_cc_start = -1;
    int ts_cc_end = -1;
    int last_ts_payload_size = 0;

    CHECK(rtpBuffer->meta()->findInt32("mode", &mode));
    CHECK(rtpBuffer->meta()->findInt32("rtp_size", &ts_pkt_count_per_rtp));
    rtp_size = RTP_HEADER_SIZE + TS_PACKET_SIZE * ts_pkt_count_per_rtp;

    mMediaSender->getRTPSessionID(&rtpSessionID);

    int64_t netstart = systemTime(SYSTEM_TIME_MONOTONIC) / 1000ll;

    for (size_t i = 0; i < rtpBuffer->size(); i += rtp_size) {
        if (rtp_size + TS_PACKET_SIZE * (7 - ts_pkt_count_per_rtp) >= remain_rtp_size)
            rtp_size = remain_rtp_size;

        mLastRTPSequenceNumber = *(rtp + 2) << 8 | *(rtp + 3);
        char *ts_data = rtp + RTP_HEADER_SIZE;
        if (mDumpFile) {
            fwrite(ts_data, 1, rtp_size - RTP_HEADER_SIZE, mDumpFile);
        }

        if (mLogLevel) {
            if (rtp_seq_num_start == -1)
                rtp_seq_num_start = rtp[2] << 8 | rtp[3];
            rtp_seq_num_end = rtp[2] << 8 | rtp[3];

            for (int k = 0 ; k < rtp_size - RTP_HEADER_SIZE; k += TS_PACKET_SIZE) {
                int ts_pid = (ts_data[1] & 0x1F) << 8 | ts_data[2];
                if (ts_pid == 0x0) { // PAT
                    ALOGI("transmitRTP(), %s, TS PAT %.2x %.2x %.2x %.2x",
                        mode == TsmuxHandler::M2M_MODE ? "M2M" : "OTF",
                        ts_data[0], ts_data[1], ts_data[2], ts_data[3]);
                    int countinuityCounter = ts_data[3] & 0xf;
                    if (mLastPatCC == -1) {
                        mLastPatCC = countinuityCounter;
                    } else {
                        if (((mLastPatCC + 1) & 0xF) != countinuityCounter)
                            ALOGI("transmitRTP(), PAT CC is discontinuous, last CC %d, cur CC %d",
                            mLastPatCC, countinuityCounter);
                        mLastPatCC = countinuityCounter;
                    }
                }
                if (ts_pid == 0x100) { // PMT
                    ALOGI("transmitRTP(), %s, TS PMT %.2x %.2x %.2x %.2x",
                        mode == TsmuxHandler::M2M_MODE ? "M2M" : "OTF",
                        ts_data[0], ts_data[1], ts_data[2], ts_data[3]);
                    int countinuityCounter = ts_data[3] & 0xf;
                    if (mLastPmtCC == -1) {
                        mLastPmtCC = countinuityCounter;
                    } else {
                        if (((mLastPmtCC + 1) & 0xF) != countinuityCounter)
                            ALOGI("transmitRTP(), PAT CC is discontinuous, last CC %d, cur CC %d",
                            mLastPmtCC, countinuityCounter);
                        mLastPmtCC = countinuityCounter;
                    }
                }
                if (ts_pid == 0x1100 || ts_pid == 0x1011) { // AAC or AVC
                    int countinuityCounter = ts_data[3] & 0xf;
                    if (ts_cc_start  == -1)
                        ts_cc_start = countinuityCounter;
                    ts_cc_end = countinuityCounter;
                    int payload_unit_start_indicator = (ts_data[1] >> 4) & 0x4;
                    if (payload_unit_start_indicator) {
                        int adaptation_field_control = (ts_data[3] >> 4) & 0x3;
                        int pes_offset = 4;
                        if (adaptation_field_control == 0x2 || adaptation_field_control == 0x3) {
                            int adaptation_field_length = ts_data[4];
                            pes_offset++;
                            pes_offset += adaptation_field_length;
                        }
                        last_ts_payload_size = 188 - pes_offset;
                        char* pes_data = ts_data + pes_offset;
                        ALOGI("transmitRTP(), %s, PTS %.2x %.2x %.2x %.2x %.2x",
                            mode == TsmuxHandler::M2M_MODE ? "M2M" : "OTF",
                            pes_data[9], pes_data[10], pes_data[11], pes_data[12], pes_data[13]);
                    }
                }
                ts_data += TS_PACKET_SIZE;
            }
        }
        mNetworkSession->sendRequest(
                rtpSessionID, rtp, rtp_size,
                true /*timeValid*/, ALooper::GetNowUs());

        rtp += rtp_size;
        remain_rtp_size -= rtp_size;
    }

    if (mLogLevel) {
        ALOGI("transmitRTP(), %s, rtp_size %zu, rtp seq start 0x%x end 0x%x, TS CC start 0x%x end 0x%x, last ts payload size %d",
            mode == TsmuxHandler::M2M_MODE ? "M2M" : "OTF",
            rtpBuffer->size(), rtp_seq_num_start, rtp_seq_num_end, ts_cc_start, ts_cc_end, last_ts_payload_size);

        if (mode == 0) {
            if (mLastAudioCC == -1) {
                mLastAudioCC = ts_cc_end;
            } else {
                if (((mLastAudioCC + 1) & 0xF) != ts_cc_start)
                    ALOGI("transmitRTP(), Audio CC is discontinuous, last CC %d, cur 1st CC %d",
                        mLastAudioCC, ts_cc_start);
                mLastAudioCC = ts_cc_end;
            }
        } else if (mode == 1) {
            if (mLastVideoCC == -1) {
                mLastVideoCC = ts_cc_end;
            } else {
                if (((mLastVideoCC + 1) & 0xF) != ts_cc_start)
                    ALOGI("transmitRTP() Video CC is discontinuous, last CC %d, cur 1st CC %d",
                        mLastVideoCC, ts_cc_start);
                mLastVideoCC = ts_cc_end;
            }
        }

        int64_t netend = systemTime(SYSTEM_TIME_MONOTONIC) / 1000ll;
        int64_t g2d_start = 0L;
        int64_t g2d_end = 0L;
        int64_t mfc_start = 0L;
        int64_t mfc_end = 0L;
        int64_t tsmux_start = 0L;
        int64_t tsmux_end = 0L;
        int64_t kernel_end = 0L;
        int64_t platform_start = 0L;
        rtpBuffer->meta()->findInt64("g2ds", &g2d_start);
        rtpBuffer->meta()->findInt64("g2de", &g2d_end);
        rtpBuffer->meta()->findInt64("mfcs", &mfc_start);
        rtpBuffer->meta()->findInt64("mfce", &mfc_end);
        rtpBuffer->meta()->findInt64("tsms", &tsmux_start);
        rtpBuffer->meta()->findInt64("tsme", &tsmux_end);
        rtpBuffer->meta()->findInt64("kere", &kernel_end);
        rtpBuffer->meta()->findInt64("plts", &platform_start);
        if (mode == TsmuxHandler::OTF_MODE && mLastG2dStart != g2d_start) {
            ALOGI("g2d %lld %lld, mfc %lld %lld, tsmux %lld %lld, krn_end %lld, plt_start %lld, network %lld %lld",
                (long long)g2d_start, (long long)g2d_end,
                (long long)mfc_start, (long long)mfc_end,
                (long long)tsmux_start, (long long)tsmux_end, (long long)kernel_end,
                (long long)platform_start, (long long)netstart, (long long)netend);
            mLastG2dStart = g2d_start;
        }
    }
}

void TsmuxSender::onMessageReceived(const sp<AMessage> &msg) {
    switch (msg->what()) {
        case kWhatInit:
        {
            ALOGV("init()");
            mRetransmitRtpVector.clear();
            mLastRTPSequenceNumber = -1;

            break;
        }
        case kWhatTransmitRTP:
        {
            sp<ABuffer> rtpBuffer;
            CHECK(msg->findBuffer("rtpBuffer", &rtpBuffer));

            if (rtpBuffer != NULL) {
                bool validRTP = true;

                if (mLastRTPSequenceNumber != -1) {
                    // check RTP sequence number
                    int mode;
                    CHECK(rtpBuffer->meta()->findInt32("mode", &mode));
                    char *rtp = (char *)rtpBuffer->data();
                    int rtpSequenceNumber = *(rtp + 2) << 8 | *(rtp + 3);
                    int expectedRtpSequenceNumber = mLastRTPSequenceNumber + 1;
                    if (expectedRtpSequenceNumber > 0xFFFF)
                        expectedRtpSequenceNumber = 0;

                    if (expectedRtpSequenceNumber != rtpSequenceNumber) {
                        mRetransmitRtpVector.push_back(rtpBuffer);
                        ALOGI("onMessageReceived(kWhatTransmitRTP) push_back %s, rtp packet, expectedRtpSeq 0x%x, rtpSeq 0x%x, mRetransmitRtpVector.size() %d",
                            mode == TsmuxHandler::M2M_MODE ? "M2M" : "OTF",
                            expectedRtpSequenceNumber, rtpSequenceNumber, (int)mRetransmitRtpVector.size());
                        validRTP = false;
                    }
                }

                if (validRTP) {
                    transmitRTP(rtpBuffer);

                    size_t vector_size = mRetransmitRtpVector.size();
                    for (size_t i = 0; i < vector_size; i++) {
                        sp<ABuffer> retransmitRTP = mRetransmitRtpVector.itemAt(0);
                        int mode;
                        CHECK(retransmitRTP->meta()->findInt32("mode", &mode));
                        char *rtp = (char *)retransmitRTP->data();
                        int rtpSequenceNumber = *(rtp + 2) << 8 | *(rtp + 3);
                        int expectedRtpSequenceNumber = mLastRTPSequenceNumber + 1;
                        if (expectedRtpSequenceNumber > 0xFFFF)
                            expectedRtpSequenceNumber = 0;
                        if (expectedRtpSequenceNumber == rtpSequenceNumber) {
                            ALOGI("onMessageReceived(kWhatTransmitRTP) retransmit %s rtp packet, rtpSeq 0x%x",
                                mode == TsmuxHandler::M2M_MODE ? "M2M" : "OTF",
                                (int)rtpSequenceNumber);
                            transmitRTP(retransmitRTP);
                            mRetransmitRtpVector.removeAt(0);
                        }
                    }
                }
                if (mRetransmitRtpVector.size() >= 10) {
                    size_t vector_size = mRetransmitRtpVector.size();
                    for (size_t i = 0; i < vector_size; i++) {
                        sp<ABuffer> retransmitRTP = mRetransmitRtpVector.itemAt(0);
                        int mode;
                        CHECK(retransmitRTP->meta()->findInt32("mode", &mode));
                        char *rtp = (char *)retransmitRTP->data();
                        int rtpSequenceNumber = *(rtp + 2) << 8 | *(rtp + 3);
                        ALOGI("onMessageReceived(kWhatTransmitRTP) force retransmit %s rtp packet, rtpSeq 0x%x",
                                mode == TsmuxHandler::M2M_MODE ? "M2M" : "OTF",
                                (int)rtpSequenceNumber);
                        transmitRTP(retransmitRTP);
                        mRetransmitRtpVector.removeAt(0);
                    }
                }
            }
            break;
        }

        default:
            TRESPASS();
    }
}

}  // namespace android
