/*
 * Copyright 2017, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef REPEATER_HANDLER_H

#define REPEATER_HANDLER_H

#include <media/stagefright/foundation/AHandler.h>
#include "MediaSender.h"

namespace android {

struct ABuffer;
struct MediaSource;
struct ANetworkSession;

struct RepeaterHandler : public AHandler {
    RepeaterHandler(void *repeaterHandle);
	void startDump();
	void stopDump();

protected:
    virtual ~RepeaterHandler();
    virtual void onMessageReceived(const sp<AMessage> &msg);

private:
    enum {
        kWhatDump,
    };

	bool mStartDump;
    void *mRepeaterHandle;
	int mFrameCount;

    DISALLOW_EVIL_CONSTRUCTORS(RepeaterHandler);
};

}  // namespace android

#endif  // REPEATER_HANDLER_H
