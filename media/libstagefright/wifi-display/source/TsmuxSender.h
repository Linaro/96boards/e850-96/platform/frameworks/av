/*
 * Copyright 2017, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef TSMUX_SENDER_H

#define TSMUX_SENDER_H

#include <media/stagefright/foundation/AHandler.h>
#include "MediaSender.h"

namespace android {

struct ABuffer;
struct MediaSource;
struct ANetworkSession;

struct TsmuxSender : public AHandler {
    TsmuxSender(sp<MediaSender> mediaSender, const sp<ANetworkSession> &netSession);

    void init();
    void sendRTP(sp<ABuffer> rtpBuffer);

    void setLogFile(FILE* logfile) {
        mDumpFile = logfile;
    }
protected:
    virtual ~TsmuxSender();
    virtual void onMessageReceived(const sp<AMessage> &msg);

private:
    enum {
        kWhatInit,
        kWhatTransmitRTP,
    };

    int mLastRTPSequenceNumber;
    sp<MediaSender> mMediaSender;
    sp<ANetworkSession> mNetworkSession;
    sp<AMessage> mNotify;

    Vector<sp<ABuffer> > mRetransmitRtpVector;

    int mLogLevel;
    int mLastPatCC;
    int mLastPmtCC;
    int mLastVideoCC;
    int mLastAudioCC;
    int64_t mLastG2dStart;

    FILE *mDumpFile;

    void transmitRTP(sp<ABuffer> rtpBuffer);

    DISALLOW_EVIL_CONSTRUCTORS(TsmuxSender);
};

}  // namespace android

#endif  // TSMUX_SENDER_H
