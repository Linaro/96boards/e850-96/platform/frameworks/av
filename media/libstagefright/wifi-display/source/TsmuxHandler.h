/*
 * Copyright 2017, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef TSMUX_HANDLER_H

#define TSMUX_HANDLER_H

#include <media/stagefright/foundation/AHandler.h>
#include "MediaSender.h"

namespace android {

struct ABuffer;
struct MediaSource;
struct ANetworkSession;
struct TsmuxSender;

struct TsmuxHandler : public AHandler {
    enum Mode {
        M2M_MODE,
        OTF_MODE,
    };

    TsmuxHandler(void *tsmuxHandle, sp<TsmuxSender> tsmuxSender,
            enum Mode mode, uint32_t width = 0, uint32_t height = 0);
    status_t startOTF();
    status_t stopOTF();
    void packetizeM2M(sp<ABuffer> &input);

protected:
    virtual ~TsmuxHandler();
    virtual void onMessageReceived(const sp<AMessage> &msg);

private:
    enum {
        kWhatStartOTF,
        kWhatStopOTF,
        kWhatRunOTF,
        kWhatPacketizeM2M,
    };

    void *mTsmuxHandle;
    enum Mode mMode;
    bool mIsStopped;
    sp<AMessage> mNotify;
	sp<TsmuxSender> mTsmuxSender;

    int64_t mLastPSITimeUs;

    uint32_t mWidth;
    uint32_t mHeight;

    Vector<sp<ABuffer> > mM2MInputBuffers;
    Mutex mLockM2MInputBuffers;

    status_t setTsmuxConfigDataOTF();
    status_t setTsmuxConfigDataM2M();

    DISALLOW_EVIL_CONSTRUCTORS(TsmuxHandler);
};

}  // namespace android

#endif  // TSMUX_HANDLER_H
